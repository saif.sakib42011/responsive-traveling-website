import React from 'react'
import HeroSection from '../../components/hero-section'
import "../../App.css";
import Card from '../../components/cards';
const Home = () => {
    return (
        <>
           <HeroSection/> 
           <Card/>
        </>
    )
}

export default Home
