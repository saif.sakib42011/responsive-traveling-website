import React from 'react'
import CardItem from './cardItem'
import './cards.css'
const Card = () => {
    return (
        <div className='cards'>
            <h1>Check Out These Epic Destination!</h1>
            <div className='cards__container'>
                <div className='cards__wrapper'>
                    <ul className='cards__items'>
                        <CardItem
                            src='/images/img-9.jpg'
                            text='Explore the hidden waterfall in deep inside the Amazon Jungle'
                            label='Adventure'
                            path='/services'
                        />

                        <CardItem
                            src='/images/img-2.jpg'
                            text='Travel to the island of Bali in a Private Cruise'
                            label='Luxury'
                            path='/services'
                        />
                    </ul>
                    <ul className='cards__items'>
                        <CardItem
                            src='/images/img-3.jpg'
                            text='Explore the hidden waterfall in deep inside the Amazon Jungle'
                            label='Mystery'
                            path='/services'
                        />
                        <CardItem
                            src='/images/img-4.jpg'
                            text='Experience Football On top of Himalaya Mountain'
                            label='Adventure'
                            path='/products'
                        />
                        <CardItem
                            src='/images/img-8.jpg'
                            text='Ride through Sahara desert on a guided camel Tour'
                            label='Addrenaline'
                            path='/sign-up'
                        />
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Card
