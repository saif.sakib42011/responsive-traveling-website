import React from 'react'
import './heroSection.css'
import "../../App.css";
import Button from '../button';
function HeroSection() {
    return (
        <div className='heroContainer'>
            <video src='/videos/video-2.mp4' autoPlay loop muted />
            <h1>Adventure Awaits</h1>
            <p>What Are You Waiting For</p>
            <div className='heroBtns'>
                <Button 
                buttonStyle='btn-outline'
                buttonSize='btn-large'
                className='btn'>
                    Get Started
                </Button>
                
                <Button 
                buttonStyle='btn-primary'
                buttonSize='btn-large'
                className='btn'>
                    Wath Later
                    <i className='far fa-play-circle'/>
                </Button>
            </div>
        </div>
    )
}

export default HeroSection
