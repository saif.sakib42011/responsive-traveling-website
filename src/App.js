import { BrowserRouter as Router,Switch,Route } from "react-router-dom";
import Navbar from "./components/navbar";
import './App.css';
import Home from "./containers/Home";
import Products from "./containers/product";
import Services from "./containers/services";
import SignUp from "./containers/signup";
function App() {
  return (
    <Router>
      <Navbar/>
      <Switch>
        <Route exact path='/' component={Home}/>
        <Route  path='/products' component={Products}/>
        <Route  path='/services' component={Services}/>
        <Route  path='/sign-up' component={SignUp}/>
      </Switch>
    </Router>
  );
}

export default App;
//https://github.com/briancodex/react-website-v1/